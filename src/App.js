import logo from './logo.svg';
import './App.css';
import React, { Component } from 'react';
import Axios from "axios"

class App extends Component {

  constructor() {
    super();

    this.state = {
      message: ""
    }
  }

  componentDidMount() {
    Axios({
      method: "GET",
      url: `${process.env.REACT_APP_API_URL}/users`,
      config: { headers: { 'Content-Type': 'multipart/form-data' } }, //New! This is a different encoding type, because we're uploading files
      withCredentials: true,
    })
      .then((response) => {
        this.setState({ message: response.data.message })
      })
      .catch((error) => {
        this.setState({ message: "Api not working." })
      })

  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <div
            className="App-link">
            {this.state.message}
          </div>
        </header>
      </div>
    );
  }
}

export default App;
